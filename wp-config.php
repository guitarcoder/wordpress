<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wordpress');
define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME']);

define('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/wp-content');
define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp-content');

define('WP_DEFAULT_THEME', 'support');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $_SERVER["RDS_DB_NAME"]);
/** MySQL database username */
define('DB_USER', $_SERVER["RDS_USERNAME"]);
/** MySQL database password */
define('DB_PASSWORD', $_SERVER["RDS_PASSWORD"]);
/** MySQL hostname */
define('DB_HOST', $_SERVER["RDS_HOSTNAME"]);
/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');
/** The Database Collate type. Don't change this if in
doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?6^-x&t[)tz!e%xvf]Y{K>Q%-)-9g{`E-%JN.C`5APl#2sAvq d5CGbtjn_g+# n');
define('SECURE_AUTH_KEY',  ' y%PFw8b@#StYQPwL;($.UMS$n@t*512Hobv}HpC}-48DR+M5&`vf0pDk~pX#tfZ');
define('LOGGED_IN_KEY',    '5xNE/KvG329_?.*mK<QU+EB+Qy!&H{O HS,ZpcKQ77r1<guElR0/ugiCbCz8[sWx');
define('NONCE_KEY',        '>mT{@Ujf$)jqMMe&a-FZY{tOtf%A,O2zO6^Uhn_+%F jh]~GD[T6Du.(bw9? 4Ws');
define('AUTH_SALT',        '~-+ O=X0Q ^eG/Z20E|?Q id4y vT3:l!4j4~`7u7L0 .=F`{@3_9[mwCXKL8(gZ');
define('SECURE_AUTH_SALT', 'OXY#bvlS@c5UU7!X!;tM2javv6(9?kDf#$^V$(+jZFEw07QKD/^DvGye_4seTzdx');
define('LOGGED_IN_SALT',   'WumEgT%o(:T6|19vr;Tej|u[4uuo+8mFQ&+Wi[iZ-U<FR~at9U-Di3gkV|i$2QZ1');
define('NONCE_SALT',       'geKCG8u`BSQ9g!mJ7fQDxXJ%,+k1zajPXt&quFIO$[l`IxG/%}9boe%Qz=qIxo,(');

/**#@-*/


// **These are restrictred access keys to give access to the S3 bucket em-cop-support** //
define( 'DBI_AWS_ACCESS_KEY_ID', 'AKIAIWRVPA6OQQMKCWRA' );
define( 'DBI_AWS_SECRET_ACCESS_KEY', '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' );


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
