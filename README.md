# Elastic Beanstalk development

## Install:

	sudo pip install awsebcli --upgrade --ignore-installed six

## Configure: 

	eb init

		Choose ap-northeast-2 (Sydney)
		Choose "EM-COP-Support"
		Choose "EM-COP-Support-dev"

The page will be visible at: 

	http://em-cop-support-dev.em.vic.gov.au/

Wordpress admin url: 

	http://em-cop-support-dev.em.vic.gov.au/wordpress/wp-admin

Wordpress Credentials are: 

	Username: emv
	Password: [removed]

## Update the dev site

To deploy an updated version of the site, first ensure changes are committed, then run:

	eb deploy

## Dealing with user uploaded content

Note this deployment also uses the plugins "Amazon Web Services" and "amazon-s3-and-cloudfront" this allows content to be uploaded via the wp-admin console. It will copy those to the em-cop-support S3 bucket and serve it out of there.

## Update the wordpress version:

All the wordpress files are in /wordpress these should remain untouched with any changes made in the /wp-content directory 
To update the wordpress version:
1. Download the latest wordpress branch from https://github.com/WordPress/WordPress
2. Delete the current /wordpress folder
3. Copy the latest wordpress version into a new /wordpress folder
4. Run "eb deploy"
5. Login to the site, it will inform you wordpress has updated and it needs to update the database. Perform this update.

# Local Development

## Install XAMPP

https://www.apachefriends.org/index.html

Change the httpd.conf to point to your git checkout directory.

	DocumentRoot "/Users/michael/Development/git.tools.vine.vic.gov.au/emcop/support"
	<Directory "/Users/michael/Development/git.tools.vine.vic.gov.au/emcop/support">
		SetEnv RDS_DB_NAME ebdb
		SetEnv RDS_USERNAME ebroot
		SetEnv RDS_PASSWORD [password]
		SetEnv RDS_HOSTNAME aa16s7vi3ix5kea.c5pstr6x9vzd.ap-southeast-2.rds.amazonaws.com
		...

